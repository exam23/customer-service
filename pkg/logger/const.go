package logger

const (
	//LevelDebug ...
	LevelDebug = "debug"
	//LevelInfo ...
	LevelInfo = "info"
	//LevelWarn ...
	LevelWarn = "warning"
	//LevelPanic ...
	LevelPanic = "panic"
	//LevelFatal ...
	LevelFatal = "fatal"
	//LevelError
	LevelError = "error"
)
