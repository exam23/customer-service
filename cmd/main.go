package main

import (
	"net"

	"gitlab.com/exam23/customer-service/config"
	pb "gitlab.com/exam23/customer-service/genproto/customer"
	"gitlab.com/exam23/customer-service/pkg/db"
	"gitlab.com/exam23/customer-service/pkg/logger"
	"gitlab.com/exam23/customer-service/service"
	grpcclient "gitlab.com/exam23/customer-service/service/grpcClient"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load()

	log := logger.New(cfg.LogLevel, "customer-service")
	defer logger.CleanUp(log)

	log.Info("main: sqlxConfig",
		logger.String("host", cfg.PostgresHost),
		logger.Int("port", cfg.PostgresPort),
		logger.String("database", cfg.PostgresDatabase))

	connDB, err := db.ConnectToDb(cfg)
	if err != nil {
		log.Fatal("Error while connecting to database", logger.Error(err))
	}
	client, err := grpcclient.New(cfg)
	if err != nil {
		log.Fatal("Error while connecting to clients", logger.Error(err))
	}
	rewiewService := service.NewCustomerService(connDB, log,client)

	lis, err := net.Listen("tcp", cfg.RPCPort)
	if err != nil {
		log.Fatal("Error while listening 1: %v", logger.Error(err))
	}

	c := grpc.NewServer()
	reflection.Register(c)
	pb.RegisterCustomerServiceServer(c, rewiewService)
	// pb(c, userService)
	log.Info("Server is running",
		logger.String("port", cfg.RPCPort))

	if err := c.Serve(lis); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}
}
