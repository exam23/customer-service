package repo

import (
	pb "gitlab.com/exam23/customer-service/genproto/customer"
)

type CustomerStorageI interface {
	Create(*pb.CreateCustomer) (*pb.Customer, error)
	Update(*pb.Customer) (*pb.Customer, error)
	Delete(*pb.Id) error
	GetCustomerI(*pb.Id) (*pb.GetCustomer, error)
	CheckField(req *pb.CheckFieldReq) (*pb.CheckFieldRes, error)
	CreateAddress(req pb.Address) (*pb.Address, error)
	GetByEmail(req *pb.LoginReq) (*pb.LoginRes, error)
	GetByRefreshToken(req *pb.RefreshTokenReq) (*pb.GetCustomer, error)
	GetList(page, limit int64) (*pb.GetListRes, error)
	GetAdmin(req *pb.GetAdminReq) (*pb.GetAdminRes, error)
	GetModerator(req *pb.GetModeratorReq) (*pb.GetModeratorRes, error)
}
