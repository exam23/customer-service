package storage

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/exam23/customer-service/storage/postgres"
	"gitlab.com/exam23/customer-service/storage/repo"
)

type IStorage interface {
	Customer() repo.CustomerStorageI
}

type StoragePg struct {
	Db           *sqlx.DB
	customerRepo repo.CustomerStorageI
}

// NewStoragePg
func NewStoragePg(db *sqlx.DB) *StoragePg {
	return &StoragePg{
		Db:           db,
		customerRepo: postgres.NewCustomerRepo(db),
	}
}

func (s StoragePg) Customer() repo.CustomerStorageI {
	return s.customerRepo
}
