package postgres

import (
	"database/sql"
	"fmt"

	pb "gitlab.com/exam23/customer-service/genproto/customer"
)

func (c *CustomerRepo) GetModerator(req *pb.GetModeratorReq) (*pb.GetModeratorRes, error) {
	res := &pb.GetModeratorRes{}
	err := c.Db.QueryRow(`SELECT 
		id,
		moderator_name, 
		created_at,
		updated_at,
		moderator_password
		from moderators where moderator_name=$1 and deleted_at is null
		`, req.Name).Scan(
		&res.Id,
		&res.Name,
		&res.CreatedAt,
		&res.UpdatedAt,
		&res.Password,
	)
	if err == sql.ErrNoRows {
		fmt.Println("No rows")
		return res, nil
	}

	if err != nil {
		return &pb.GetModeratorRes{}, err
	}
	return res, nil
}
