package postgres

import (
	"github.com/jmoiron/sqlx"
)

type CustomerRepo struct {
	Db *sqlx.DB
}

func NewCustomerRepo(db *sqlx.DB) *CustomerRepo {
	return &CustomerRepo{Db: db}
}