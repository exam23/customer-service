package postgres

import (
	"database/sql"
	"fmt"

	pb "gitlab.com/exam23/customer-service/genproto/customer"
)

func (c *CustomerRepo) GetAdmin(req *pb.GetAdminReq) (*pb.GetAdminRes, error) {
	res := &pb.GetAdminRes{}
	err := c.Db.QueryRow(`SELECT 
		id,
		admin_name, 
		created_at,
		updated_at,
		admin_password
		from admins where admin_name=$1 and deleted_at is null
		`, req.Name).Scan(
		&res.Id,
		&res.Name,
		&res.CreatedAt,
		&res.UpdatedAt,
		&res.Password,
	)
	if err == sql.ErrNoRows {
		fmt.Println("No rows")
		return res, nil
	}

	if err != nil {
		return &pb.GetAdminRes{}, err
	}
	return res, nil
}
