package postgres

import (
	"database/sql"
	"fmt"

	pb "gitlab.com/exam23/customer-service/genproto/customer"
)

func (c *CustomerRepo) Create(req *pb.CreateCustomer) (*pb.Customer, error) {
	res := &pb.Customer{}
	err := c.Db.QueryRow(`INSERT INTO customers (
		id,
		first_name,
		last_name,
		bio,
		email,
		phone_number,
		password_c,
		refresh_token
	) values($1, $2, $3, $4, $5, $6, $7, $8) returning
		id,
		first_name,
		last_name,
		bio,
		email,
		phone_number,
		updated_at,
		created_at
	`, req.Id, req.FirstName, req.LastName, req.Bio,
		req.Email, req.PhoneNumber, req.Password, req.RefreshToken).Scan(
		&res.Id,
		&res.FirstName,
		&res.LastName,
		&res.Bio,
		&res.Email,
		&res.PhoneNumber,
		&res.UpdatedAt,
		&res.CreatedAt,
	)
	if err != nil {
		return nil, err
	}
	for _, address := range req.Addresses {
		address.OwnerId = res.Id

		add, err := c.CreateAddress(*address)
		if err != nil {
			return nil, err
		}
		res.Addresses = append(res.Addresses, add)
	}

	return res, nil
}

func (c *CustomerRepo) Update(req *pb.Customer) (*pb.Customer, error) {
	err := c.Db.QueryRow(
		`UPDATE customers SET updated_at=NOW(),
		first_name=$1,
		last_name=$2,
		bio=$3,
		email=$4,
		phone_number=$5
		WHERE id=$6 and deleted_at is null
		returning
		updated_at,
		created_at
		`,
		req.FirstName,
		req.LastName,
		req.Bio,
		req.Email,
		req.PhoneNumber,
		req.Id,
	).Scan(
		&req.UpdatedAt,
		&req.CreatedAt,
	)

	if err != nil {
		return nil, err
	}
	addresses := []*pb.Address{}
	for _, a := range req.Addresses {
		add, err := c.UpateAddress(a)
		if err != nil {
			return nil, err
		}
		addresses = append(addresses, add)
	}
	req.Addresses = addresses
	return req, err
}

func (c *CustomerRepo) Delete(req *pb.Id) error {
	err := c.Db.QueryRow(`update customers SET deleted_at = NOW() 
	WHERE id=$1 and deleted_at is null`, req.Id).Err()
	if err != nil {
		return err
	}
	err = c.DeleteAddress(req)
	return err
}

func (c *CustomerRepo) GetCustomerI(req *pb.Id) (*pb.GetCustomer, error) {
	res := &pb.GetCustomer{}
	err := c.Db.QueryRow(`SELECT 
		id,
		first_name,
		last_name,
		bio,
		email,
		phone_number,
		created_at, 
		updated_at
	FROM customers WHERE id=$1 and deleted_at is null`, req.Id).Scan(
		&res.Id,
		&res.FirstName,
		&res.LastName,
		&res.Bio,
		&res.Email,
		&res.PhoneNumber,
		&res.CreatedAt,
		&res.UpdatedAt,
	)
	if err != nil {
		return nil, err
	}
	res.Addresses, err = c.GetAddress(req)
	return res, err
}

func (r *CustomerRepo) CheckField(req *pb.CheckFieldReq) (*pb.CheckFieldRes, error) {
	query := fmt.Sprintf("SELECT 1 FROM customers WHERE %s=$1", req.Field)
	res := &pb.CheckFieldRes{}
	temp := -1
	err := r.Db.QueryRow(query, req.Value).Scan(&temp)
	if err != nil {
		res.Exists = false
		return res, nil
	}

	if temp == 0 {
		res.Exists = true
	} else {
		res.Exists = false
	}
	return res, nil
}

func (r *CustomerRepo) GetByEmail(req *pb.LoginReq) (*pb.LoginRes, error) {
	res := &pb.LoginRes{}
	err := r.Db.QueryRow(`SELECT 
		id,
		first_name,
		last_name,
		bio,
		email,
		password_c,
		phone_number,
		created_at, 
		updated_at,
		refresh_token
	FROM customers WHERE email=$1 and deleted_at is null`, req.Email).Scan(
		&res.Id,
		&res.FirstName,
		&res.LastName,
		&res.Bio,
		&res.Email,
		&res.Password,
		&res.PhoneNumber,
		&res.CreatedAt,
		&res.UpdatedAt,
		&res.Refreshtoken,
	)
	if err == sql.ErrNoRows {
		return &pb.LoginRes{}, nil
	}
	res.Addresses, err = r.GetAddress(&pb.Id{Id: res.Id})
	return res, err
}

func (r *CustomerRepo) GetByRefreshToken(req *pb.RefreshTokenReq) (*pb.GetCustomer, error) {
	res := &pb.GetCustomer{}
	err := r.Db.QueryRow(`SELECT 
		id,
		first_name,
		last_name,
		bio,
		email,
		phone_number,
		created_at, 
		updated_at
	FROM customers WHERE refresh_token=$1 and deleted_at is null`, req.Token).Scan(
		&res.Id,
		&res.FirstName,
		&res.LastName,
		&res.Bio,
		&res.Email,
		&res.PhoneNumber,
		&res.CreatedAt,
		&res.UpdatedAt,
	)
	if err == sql.ErrNoRows {
		return &pb.GetCustomer{}, nil
	}
	return res, err
}

func (r *CustomerRepo) GetList(page, limit int64) (*pb.GetListRes, error) {
	offset := (page - 1) * limit
	rows, err := r.Db.Query(`SELECT 
		id,
		first_name,
		last_name,
		bio,
		email,
		phone_number,
		created_at, 
		updated_at
	FROM customers 
	WHERE deleted_at is null
	LIMIT $1
	OFFSET $2
	`, limit, offset)
	if err != nil {
		return &pb.GetListRes{}, err
	}
	defer rows.Close()

	response := &pb.GetListRes{}
	for rows.Next() {
		temp := &pb.GetCustomer{}
		rows.Scan(
			&temp.Id,
			&temp.FirstName,
			&temp.LastName,
			&temp.Bio,
			&temp.Email,
			&temp.PhoneNumber,
			&temp.CreatedAt,
			&temp.UpdatedAt,
		)
		temp.Addresses, err = r.GetAddress(&pb.Id{Id: temp.Id})
		if err != nil {
			return &pb.GetListRes{}, err
		}
		response.Customers = append(response.Customers, temp)
	}
	return response, nil
}