package postgres

import (
	"database/sql"

	pb "gitlab.com/exam23/customer-service/genproto/customer"
)

func (c *CustomerRepo) CreateAddress(req pb.Address) (*pb.Address, error) {
	res := &pb.Address{}
	err := c.Db.QueryRow(`
		INSERT INTO addresses(
			owner_id,
			district,
			street,
			home_number
		) values($1, $2, $3, $4)
		returning 
			id,
			owner_id,
			district,
			street,
			home_number,
			created_at, 
			updated_at
	`, req.OwnerId,
		req.District,
		req.Street,
		req.HomeNumber).Scan(
		&res.Id,
		&res.OwnerId,
		&res.District,
		&res.Street,
		&res.HomeNumber,
		&res.CreatedAt,
		&res.UpdatedAt,
	)
	return res, err
}

func (c *CustomerRepo) GetAddress(id *pb.Id) ([]*pb.Address, error) {
	response := []*pb.Address{}
	rows, err := c.Db.Query(`SELECT 
		id,
		owner_id,
		district,
		street,
		home_number, 
		created_at,
		updated_at
		FROM addresses where owner_id=$1 and deleted_at is null`, id.Id)

	if err != nil {
		return []*pb.Address{}, err
	}
	defer rows.Close()

	for rows.Next() {
		temp := &pb.Address{}
		err = rows.Scan(
			&temp.Id,
			&temp.OwnerId,
			&temp.District,
			&temp.Street,
			&temp.HomeNumber,
			&temp.CreatedAt,
			&temp.UpdatedAt,
		)
		if err == sql.ErrNoRows {
			return []*pb.Address{}, nil
		}
		if err != nil {
			return []*pb.Address{}, err
		}
		response = append(response, temp)
	}

	return response, err
}

func (c *CustomerRepo) DeleteAddress(id *pb.Id) error {
	return c.Db.QueryRow(`UPDATE addresses SET updated_at = NOW() 
		WHERE owner_id = $1 and deleted_at is null`, id.Id).Err()
}

func (c *CustomerRepo) UpateAddress(req *pb.Address) (*pb.Address, error) {
	res := &pb.Address{}
	err := c.Db.QueryRow(`
		UPDATE addresses SET 
			updated_at=NOW(),
			district=$1,
			street=$2,
			home_number=$3
		WHERE id=$4 and deleted_at is null
		returning
			id,
			owner_id,
			district,
			street,
			home_number,
			updated_at,
			created_at
	`,
		req.District,
		req.Street,
		req.HomeNumber,
		req.Id,
	).Scan(
		&res.Id,
		&res.OwnerId,
		&res.District,
		&res.Street,
		&res.HomeNumber,
		&res.UpdatedAt,
		&res.CreatedAt,
	)
	return res, err
}
