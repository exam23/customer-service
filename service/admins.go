package service

import (
	"context"

	pb "gitlab.com/exam23/customer-service/genproto/customer"
	"gitlab.com/exam23/customer-service/pkg/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *CustomerService) GetAdmin(ctx context.Context, req *pb.GetAdminReq) (*pb.GetAdminRes, error) {
	res, err := s.Storage.Customer().GetAdmin(req)
	if err != nil {
		s.Logger.Error("Error while getting addmin by adminname", logger.Any("get", err))
		return &pb.GetAdminRes{}, status.Error(codes.NotFound, "Your are not admin")
	}
	return res, nil
}
