package service

import (
	"context"

	pb "gitlab.com/exam23/customer-service/genproto/customer"
	"gitlab.com/exam23/customer-service/genproto/post"
	"gitlab.com/exam23/customer-service/pkg/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *CustomerService) Create(ctx context.Context, req *pb.CreateCustomer) (*pb.Customer, error) {
	res, err := s.Storage.Customer().Create(req)
	if err != nil {
		s.Logger.Error("Error while creating customer", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, "Please check your data")
	}
	return res, nil
}

func (s *CustomerService) Update(ctx context.Context, req *pb.Customer) (*pb.Customer, error) {
	res, err := s.Storage.Customer().Update(req)
	if err != nil {
		s.Logger.Error("Error while updating customer", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, "Please check your data")
	}
	return res, nil
}

func (s *CustomerService) Delete(ctx context.Context, req *pb.Id) (*pb.Empty, error) {
	err := s.Storage.Customer().Delete(req)
	if err != nil {
		s.Logger.Error("Error while deleting customer", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, "Something went wrong")
	}
	_, err = s.Client.Post().DeleteByOwnerId(ctx, &post.Id{Id: req.Id})
	if err != nil {
		s.Logger.Error("Error while deleting customer posts", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, "Something went wrong")
	}
	// _, err = s.Client.Rewiew().DeleteByCustomerId(ctx, &rewiew.Id{Id: req.Id})
	// if err != nil {
	// 	s.Logger.Error("Error while deleting customer posts", logger.Error(err))
	// 	return nil, status.Error(codes.InvalidArgument, "Something went wrong")
	// }
	return &pb.Empty{}, nil
}

// This function returns full customer info
func (s *CustomerService) GetCustomerInfo(ctx context.Context, req *pb.Id) (*pb.GetCustomer, error) {
	res, err := s.Storage.Customer().GetCustomerI(req)
	if err != nil {
		s.Logger.Error("Error while getting customer info", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, "Something went wrong")
	}
	posts, err := s.Client.Post().GetByOwnerId(ctx, &post.Id{Id: res.Id})
	if err != nil {
		s.Logger.Error("Error while getting customer posts", logger.Error(err))
		// return nil, status.Error(codes.InvalidArgument, "Something went wrong")
		posts = &post.GetPosts{}
	}

	for _, p := range posts.Posts {
		temp := &pb.GetPost{
			Id:          p.Id,
			OwnerId:     res.Id,
			Title:       p.Title,
			Description: p.Description,
			Content:     p.Content,
			CreatedAt:   p.CreatedAt,
			UpdatedAt:   p.UpdatedAt,
		}
		for _, media := range p.Medias {
			temp.Medias = append(temp.Medias, (*pb.Media)(media))
		}
		for _, rewiew := range p.Rewiews {
			temp.Rewiews = append(temp.Rewiews, (*pb.RewiewRes)(rewiew))
		}
		res.Posts = append(res.Posts, temp)
	}
	res.CountPosts = posts.Count
	return res, nil
}

// This function returns customer info without posts
func (s *CustomerService) GetCustomerI(ctx context.Context, req *pb.Id) (*pb.Customer, error) {
	res, err := s.Storage.Customer().GetCustomerI(req)
	if err != nil {
		s.Logger.Error("Error while getting customerI info", logger.Error(err))
		return &pb.Customer{}, status.Error(codes.InvalidArgument, "Something went wrong")
	}
	response := &pb.Customer{
		Id:          res.Id,
		FirstName:   res.FirstName,
		LastName:    res.LastName,
		Bio:         res.Bio,
		Email:       res.Email,
		PhoneNumber: res.PhoneNumber,
		Addresses:   res.Addresses,
		CreatedAt:   res.CreatedAt,
		UpdatedAt:   res.UpdatedAt,
	}
	return response, nil
}

func (s *CustomerService) AddAddress(ctx context.Context, req *pb.Address) (*pb.Address, error) {
	res, err := s.Storage.Customer().CreateAddress(*req)
	return res, err
}

func (u *CustomerService) CheckField(ctx context.Context, req *pb.CheckFieldReq) (*pb.CheckFieldRes, error) {
	res, err := u.Storage.Customer().CheckField(req)
	if err != nil {
		u.Logger.Error("Error while checking field", logger.Any("Select", err))
		return nil, status.Error(codes.Internal, "Please check your data")
	}
	return res, nil
}

func (s *CustomerService) GetByEmail(ctx context.Context, req *pb.LoginReq) (*pb.LoginRes, error) {
	res, err := s.Storage.Customer().GetByEmail(req)
	if err != nil {
		s.Logger.Error("Error while getting customer info by email", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, "Something went wrong")
	}
	posts, err := s.Client.Post().GetByOwnerId(ctx, &post.Id{Id: res.Id})
	if err != nil {
		s.Logger.Error("Error while getting customer posts", logger.Error(err))
		// return nil, status.Error(codes.InvalidArgument, "Something went wrong")
		posts = &post.GetPosts{}
	}

	for _, p := range posts.Posts {
		temp := &pb.GetPost{
			Id:          p.Id,
			OwnerId:     res.Id,
			Title:       p.Title,
			Description: p.Description,
			Content:     p.Content,
			CreatedAt:   p.CreatedAt,
			UpdatedAt:   p.UpdatedAt,
		}
		for _, media := range p.Medias {
			temp.Medias = append(temp.Medias, (*pb.Media)(media))
		}
		for _, rewiew := range p.Rewiews {
			temp.Rewiews = append(temp.Rewiews, (*pb.RewiewRes)(rewiew))
		}
		res.Posts = append(res.Posts, temp)
	}
	res.CountPosts = posts.Count
	return res, nil
}

func (s *CustomerService) GetByRefreshToken(ctx context.Context, req *pb.RefreshTokenReq) (*pb.GetCustomer, error) {
	res, err := s.Storage.Customer().GetByRefreshToken(req)
	if err != nil {
		s.Logger.Error("Error while getting customer info by Refresh Token", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, "Something went wrong")
	}
	posts, err := s.Client.Post().GetByOwnerId(ctx, &post.Id{Id: res.Id})
	if err != nil {
		s.Logger.Error("Error while getting customer posts", logger.Error(err))
		// return nil, status.Error(codes.InvalidArgument, "Something went wrong")
		posts = &post.GetPosts{}
	}

	for _, p := range posts.Posts {
		temp := &pb.GetPost{
			Id:          p.Id,
			OwnerId:     res.Id,
			Title:       p.Title,
			Description: p.Description,
			Content:     p.Content,
			CreatedAt:   p.CreatedAt,
			UpdatedAt:   p.UpdatedAt,
		}
		for _, media := range p.Medias {
			temp.Medias = append(temp.Medias, (*pb.Media)(media))
		}
		for _, rewiew := range p.Rewiews {
			temp.Rewiews = append(temp.Rewiews, (*pb.RewiewRes)(rewiew))
		}
		res.Posts = append(res.Posts, temp)
	}
	res.CountPosts = posts.Count
	return res, nil
}

func (s *CustomerService) GetList(ctx context.Context, req *pb.GetListReq) (*pb.GetListRes, error) {
	res, err := s.Storage.Customer().GetList(req.Page, req.Limit)
	if err != nil {
		s.Logger.Error("Error while getting customer list", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, "Something went wrong")
	}

	l := len(res.Customers)
	for i := 0; i < l; i++ {
		cust := res.Customers[i]
		posts, err := s.Client.Post().GetByOwnerId(ctx, &post.Id{Id: cust.Id})
		if err != nil {
			s.Logger.Error("Error while getting customer posts", logger.Error(err))
			// return nil, status.Error(codes.InvalidArgument, "Something went wrong")
			posts = &post.GetPosts{}
		}

		for _, p := range posts.Posts {
			temp := &pb.GetPost{
				Id:          p.Id,
				OwnerId:     cust.Id,
				Title:       p.Title,
				Description: p.Description,
				Content:     p.Content,
				CreatedAt:   p.CreatedAt,
				UpdatedAt:   p.UpdatedAt,
			}
			for _, media := range p.Medias {
				temp.Medias = append(temp.Medias, (*pb.Media)(media))
			}
			for _, rewiew := range p.Rewiews {
				temp.Rewiews = append(temp.Rewiews, (*pb.RewiewRes)(rewiew))
			}
			res.Customers[i].Posts = append(res.Customers[i].Posts, temp)
		}
		res.Customers[i].CountPosts = posts.Count
	}

	return res, nil
}
