package service

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/exam23/customer-service/pkg/logger"
	grpcclient "gitlab.com/exam23/customer-service/service/grpcClient"
	"gitlab.com/exam23/customer-service/storage"
)

type CustomerService struct {
	Storage storage.IStorage
	Logger  logger.Logger
	Client  grpcclient.Cleints
}

func NewCustomerService(db *sqlx.DB, l logger.Logger, client grpcclient.Cleints) *CustomerService {
	return &CustomerService{
		Storage: storage.NewStoragePg(db),
		Logger:  l,
		Client: client,
	}
}


