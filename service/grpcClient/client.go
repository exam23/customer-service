package grpcclient

import (
	"fmt"

	"gitlab.com/exam23/customer-service/config"
	ps "gitlab.com/exam23/customer-service/genproto/post"
	rs "gitlab.com/exam23/customer-service/genproto/rewiew"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type Cleints interface {
	Rewiew() rs.RewiewServiceClient
	Post() ps.PostServiceClient
}
type ServiceManager struct {
	config        config.Config
	rewiewService rs.RewiewServiceClient
	postService   ps.PostServiceClient
}

func New(c config.Config) (Cleints, error) {
	rewiew, err := grpc.Dial(
		fmt.Sprintf("%s:%d", c.RewiewServiceHost, c.RewiewServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return &ServiceManager{}, nil
	}
	post, err := grpc.Dial(
		fmt.Sprintf("%s:%d", c.PostServiceHost, c.PostServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return &ServiceManager{}, nil
	}
	return &ServiceManager{
		config:        c,
		rewiewService: rs.NewRewiewServiceClient(rewiew),
		postService:   ps.NewPostServiceClient(post),
	}, nil
}

func (s *ServiceManager) Rewiew() rs.RewiewServiceClient {
	return s.rewiewService
}

func (s *ServiceManager) Post() ps.PostServiceClient {
	return s.postService
}
