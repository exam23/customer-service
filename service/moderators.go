package service

import (
	"context"

	pb "gitlab.com/exam23/customer-service/genproto/customer"
	"gitlab.com/exam23/customer-service/pkg/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *CustomerService) GetModerator(ctx context.Context, req *pb.GetModeratorReq) (*pb.GetModeratorRes, error) {
	res, err := s.Storage.Customer().GetModerator(req)
	if err != nil {
		s.Logger.Error("Error while getting moderator by moderator", logger.Any("get", err))
		return &pb.GetModeratorRes{}, status.Error(codes.NotFound, "Your are not moderator")
	}
	return res, nil
}
