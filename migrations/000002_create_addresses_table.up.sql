CREATE TABLE IF NOT EXISTS addresses (
    owner_id UUID NOT NULL REFERENCES customers(id),
    district TEXT NOT NULL,
    street TEXT NOT NULL,
    home_number VARCHAR(50) NOT NULL,
    created_at TIME NOT NULL DEFAULT NOW(), 
    updated_at TIME NOT NULL DEFAULT NOW(),
    deleted_at TIME
);