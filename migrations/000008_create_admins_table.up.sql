CREATE TABLE IF NOT EXISTS admins (
    id uuid NOT NULL PRIMARY KEY,
    admin_name TEXT NOT NULL,
    admin_password TEXT NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT now(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT now(),
    deleted_at TIMESTAMPTZ
);