ALTER TABLE addresses DROP COLUMN created_at;
ALTER TABLE addresses DROP COLUMN updated_at;
ALTER TABLE addresses DROP COLUMN deleted_at;

ALTER TABLE addresses ADD COLUMN created_at TIMESTAMPTZ;
ALTER TABLE addresses ALTER COLUMN created_at SET DEFAULT now();

ALTER TABLE addresses ADD COLUMN updated_at TIMESTAMPTZ;
ALTER TABLE addresses ALTER COLUMN updated_at SET DEFAULT now();

ALTER TABLE addresses ADD COLUMN deleted_at TIMESTAMPTZ;
