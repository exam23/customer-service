ALTER TABLE customers DROP COLUMN created_at;
ALTER TABLE customers DROP COLUMN updated_at;
ALTER TABLE customers DROP COLUMN created_at;

ALTER TABLE customers ADD COLUMN created_at TIME NOT NULL DEFAULT NOW();
ALTER TABLE customers ADD COLUMN updated_at TIME NOT NULL DEFAULT NOW();
ALTER TABLE customers ADD COLUMN deleted_at TIME;
