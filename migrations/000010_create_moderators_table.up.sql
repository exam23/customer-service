CREATE TABLE IF NOT EXISTS moderators (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    moderator_name TEXT NOT NULL,
    moderator_password TEXT NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT now(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT now(),
    deleted_at TIMESTAMPTZ
);