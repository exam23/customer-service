CREATE TABLE IF NOT EXISTS customers (
    id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
    first_name TEXT NOT NULL,
    last_name TEXT,
    bio TEXT,
    email VARCHAR(100) UNIQUE, 
    phone_number VARCHAR(100) NOT NULL UNIQUE,
    created_at TIME NOT NULL DEFAULT NOW(), 
    updated_at TIME NOT NULL DEFAULT NOW(),
    deleted_at TIME
);