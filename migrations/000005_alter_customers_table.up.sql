ALTER TABLE customers DROP COLUMN created_at;
ALTER TABLE customers DROP COLUMN updated_at;
ALTER TABLE customers DROP COLUMN deleted_at;

ALTER TABLE customers ADD COLUMN created_at TIMESTAMPTZ;
ALTER TABLE customers ALTER COLUMN created_at SET DEFAULT now();

ALTER TABLE customers ADD COLUMN updated_at TIMESTAMPTZ;
ALTER TABLE customers ALTER COLUMN updated_at SET DEFAULT now();

ALTER TABLE customers ADD COLUMN deleted_at TIMESTAMPTZ;
